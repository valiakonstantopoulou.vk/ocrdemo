import cv2
import numpy as np


def is_horizontal(x1, y1, x2, y2):
    return True if x2 - x1 > y2 - y1 else False


def is_big_line(x1, x2):
    return True if x2 - x1 > 20 else False


def detect_lines(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    cv2.imshow('Gray Image', gray)
    cv2.waitKey(0)
    cv2.destroyWindow('Gray Image')
    edges = cv2.Canny(gray, 50, 150, apertureSize=3)
    cv2.imshow('Canny Image', edges)
    cv2.waitKey(0)
    cv2.destroyWindow('Canny Image')
    minLineLength = 1000  # 100
    maxLineGap = 5
    lines = cv2.HoughLinesP(edges, 1, np.pi / 180, 100, minLineLength, maxLineGap)
    for line in lines:
        for x1, y1, x2, y2 in line:
            if is_horizontal(x1, y1, x2, y2):
                if is_big_line(x1, x2):
                    cv2.line(image, (x1, y1), (x2, y2), (0, 255, 0), 2)
    cv2.imshow('Lines', image)
    cv2.waitKey(0)
    cv2.destroyWindow('Lines')


def detect_contours(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (7, 7), 0)
    cv2.imshow('GaussianBlur', blur)
    cv2.waitKey(0)
    cv2.destroyWindow('GaussianBlur')
    thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
    cv2.imshow('Threshold', thresh)
    cv2.waitKey(0)
    cv2.destroyWindow('Threshold')
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,
                                       (5, 5))
    dilate = cv2.dilate(thresh, kernel, iterations=4)
    cv2.imshow('Dilated', dilate)
    cv2.waitKey(0)
    cv2.destroyWindow('Dilated')

    cnts = cv2.findContours(dilate, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]

    for c in reversed(cnts):
        x, y, w, h = cv2.boundingRect(c)
        cv2.rectangle(image, (x, y), (x + w, y + h), (8, 8, 7), 1)
        cropped_image = image[y:y + h, x:x + w]
        cv2.imshow('Contours', cropped_image)
        cv2.waitKey(0)
        cv2.destroyWindow('Contours')


if __name__ == '__main__':
    img = '/Users/valia/Desktop/Article.png'
    image = cv2.imread(img)
    cv2.imshow('Original Image', image)
    cv2.waitKey(0)
    cv2.destroyWindow('Original Image')

    detect_lines(image)
    detect_contours(image)
